package bugfix;

import etwin.Error;
import etwin.Obfu;
import patchman.module.Data;

@:build(patchman.Build.di())
class BugfixConfig {
  /**
  Prevent players from getting stuck on top of a lateral portal field.
  **/
  public var canBeStuckOnTopOfLateralVortices(default, null): Bool;

  /**
  Prevents music from restarting if the same music (same id) is played twice in a row.
  **/
  public var startAgainMusicIfSameID(default, null): Bool;

  /**
  Fix the `lifesharing` option.
  **/
  public var lifeSharingInMulti(default, null): Bool;

  /**
  Prevent darkness from being applied multiplied times in multiplayer mode.
  **/
  public var reducedDarknessInMulti(default, null): Bool;

  /**
  Fix application of multiple extra lives from score steps.
  **/
  public var consecutiveScoreLevels(default, null): Bool;

  /**
  Prevent lychees from breaking the order.
  **/
  public var lycheesBreakOrder(default, null): Bool;

  /**
  Reduce lag in long runs.
  **/
  public var progressiveLagInLongRuns(default, null): Bool;

  /**
   Ensure strawberries can't miss when throwing their ball.
   */
  public var strawberriesCanMissEachOther(default, null): Bool;

  /**
  Remove invisible portals left by `openPortal` after exiting a level.  
  
  WARNING: This fix won't work if Merlin < v0.14.2 is used.
  **/
  public var invisiblePortalsWhenReenteringLevel(default, null): Bool;

  /**
  Fix player getting stuck in certain animations when changing disguises while controls are locked.
  **/
  public var loopingAnimationsWhenChangingDisguisesCanSoftlock(default, null): Bool;

  /**
  Fix certain animations looping indefinitely when changing disguises.
  **/
  public var loopingAnimationsWhenChangingDisguises(default, null): Bool;

  /**
  Prevents the player from being hit by explosions and projectiles while warping.
  **/
  public var intangibleWhileWarpingEIBombs(default, null): Bool;
  public var intangibleWhileWarpingRedBombs(default, null): Bool;
  public var intangibleWhileWarpingHits(default, null): Bool;

  /**
  Fix player activating triggers and fields at the previous level's position after a warp.
  **/
  public var playerPositionAfterWarp(default, null): Bool;

  /**
  Prevents the Pocket Guu rain from being disabled when not enabling the graphical details.
  **/
  public var keepPocketGuuRain(default, null): Bool;

  /**
  Fix fields at the player spawn.
  **/
  public var raysAtRespawn(default, null): Bool;

  /**
  Fix off-by-one indicators in map screen when entering a land.
  **/
  public var mapScreenIndicators(default, null): Bool;

  /**
  Fix fields not working in the top row (`y == 0`)
  **/
  public var raysOnHighestLine(default, null): Bool;

  /**
  Fix teleport fields not working in the top row (`y == 0`)
  **/
  public var teleportersOnHighestLine(default, null): Bool;

  /**
  Fix fruits stuck in a 1 case wide column.
  **/
  public var fruitsStuckInColumns(default, null): Bool;

  /**
  Fix spawn point of portal fields in the bottom row.
  **/
  public var respawnOfLateralVorticesOnLowestLine(default, null): Bool;

  /**
  Prevent bads from jumping if there is an obstacle on the way.
  **/
  public var fruitsCanJumpIntoAPlatform(default, null): Bool;

  /**
  Fix movement of wall walker bads over the top of the level.
  **/
  public var crawlingBadsOnHighestLine(default, null): Bool;

  /**
  Displays the "new land" annotation even at the start of supersets.
  **/
  public var displayNewLandOnFirstLevel(default, null): Bool;

  public static var DEFAULT(default, null): BugfixConfig = new BugfixConfig({
    canBeStuckOnTopOfLateralVortices: true,
    startAgainMusicIfSameID: true,
    lifeSharingInMulti: true,
    reducedDarknessInMulti: true,
    consecutiveScoreLevels: true,
    lycheesBreakOrder: true,
    progressiveLagInLongRuns: true,
    strawberriesCanMissEachOther: true,
    invisiblePortalsWhenReenteringLevel: true,
    loopingAnimationsWhenChangingDisguisesCanSoftlock: true,
    loopingAnimationsWhenChangingDisguises: false,
    intangibleWhileWarpingEIBombs: true,
    intangibleWhileWarpingRedBombs: true,
    intangibleWhileWarpingHits: true,
    playerPositionAfterWarp: true,
    keepPocketGuuRain: true,
    mapScreenIndicators: true,
    raysAtRespawn: false,
    raysOnHighestLine: false,
    teleportersOnHighestLine: false,
    fruitsStuckInColumns: false,
    respawnOfLateralVorticesOnLowestLine: false,
    fruitsCanJumpIntoAPlatform: false,
    crawlingBadsOnHighestLine: false,
    displayNewLandOnFirstLevel: false,
  });

  private function new(options: Options): Void {
    this.canBeStuckOnTopOfLateralVortices = options.canBeStuckOnTopOfLateralVortices;
    this.startAgainMusicIfSameID = options.startAgainMusicIfSameID;
    this.lifeSharingInMulti = options.lifeSharingInMulti;
    this.reducedDarknessInMulti = options.reducedDarknessInMulti;
    this.consecutiveScoreLevels = options.consecutiveScoreLevels;
    this.lycheesBreakOrder = options.lycheesBreakOrder;
    this.progressiveLagInLongRuns = options.progressiveLagInLongRuns;
    this.strawberriesCanMissEachOther = options.strawberriesCanMissEachOther;
    this.invisiblePortalsWhenReenteringLevel = options.invisiblePortalsWhenReenteringLevel;
    this.loopingAnimationsWhenChangingDisguisesCanSoftlock = options.loopingAnimationsWhenChangingDisguisesCanSoftlock;
    this.loopingAnimationsWhenChangingDisguises = options.loopingAnimationsWhenChangingDisguises;
    this.intangibleWhileWarpingEIBombs = options.intangibleWhileWarpingEIBombs;
    this.intangibleWhileWarpingRedBombs = options.intangibleWhileWarpingRedBombs;
    this.intangibleWhileWarpingHits = options.intangibleWhileWarpingHits;
    this.playerPositionAfterWarp = options.playerPositionAfterWarp;
    this.keepPocketGuuRain = options.keepPocketGuuRain;
    this.mapScreenIndicators = options.mapScreenIndicators;
    this.raysAtRespawn = options.raysAtRespawn;
    this.raysOnHighestLine = options.raysOnHighestLine;
    this.teleportersOnHighestLine = options.teleportersOnHighestLine;
    this.fruitsStuckInColumns = options.fruitsStuckInColumns;
    this.respawnOfLateralVorticesOnLowestLine = options.respawnOfLateralVorticesOnLowestLine;
    this.fruitsCanJumpIntoAPlatform = options.fruitsCanJumpIntoAPlatform;
    this.crawlingBadsOnHighestLine = options.crawlingBadsOnHighestLine;
    this.displayNewLandOnFirstLevel = options.displayNewLandOnFirstLevel;
  }

  @:diFactory
  public static function fromHml(dataMod: Data): BugfixConfig {
    if (!dataMod.has(Obfu.raw("BugFix"))) {
      return BugfixConfig.DEFAULT;
    }

    return BugfixConfig.fromRaw(dataMod.get(Obfu.raw("BugFix")));
  }

  public static function fromRaw(data: Dynamic): BugfixConfig {
    if (!Reflect.isObject(data)) {
      throw new Error("TypeError: BugfixConfig should be an object");
    }

    var baseConfig = BugfixConfig.DEFAULT;
    return new BugfixConfig({
      canBeStuckOnTopOfLateralVortices: BugfixConfig.getBoolOr(data, Obfu.raw("canBeStuckOnTopOfLateralVortices"), baseConfig.canBeStuckOnTopOfLateralVortices),
      startAgainMusicIfSameID: BugfixConfig.getBoolOr(data, Obfu.raw("startAgainMusicIfSameID"), baseConfig.startAgainMusicIfSameID),
      lifeSharingInMulti: BugfixConfig.getBoolOr(data, Obfu.raw("lifeSharingInMulti"), baseConfig.lifeSharingInMulti),
      reducedDarknessInMulti: BugfixConfig.getBoolOr(data, Obfu.raw("reducedDarknessInMulti"), baseConfig.reducedDarknessInMulti),
      consecutiveScoreLevels: BugfixConfig.getBoolOr(data, Obfu.raw("consecutiveScoreLevels"), baseConfig.consecutiveScoreLevels),
      lycheesBreakOrder: BugfixConfig.getBoolOr(data, Obfu.raw("lycheesBreakOrder"), baseConfig.lycheesBreakOrder),
      progressiveLagInLongRuns: BugfixConfig.getBoolOr(data, Obfu.raw("progressiveLagInLongRuns"), baseConfig.progressiveLagInLongRuns),
      strawberriesCanMissEachOther: BugfixConfig.getBoolOr(data, Obfu.raw("strawberriesCanMissEachOther"), baseConfig.strawberriesCanMissEachOther),
      invisiblePortalsWhenReenteringLevel: BugfixConfig.getBoolOr(data, Obfu.raw("invisiblePortalsWhenReenteringLevel"), baseConfig.invisiblePortalsWhenReenteringLevel),
      loopingAnimationsWhenChangingDisguisesCanSoftlock: BugfixConfig.getBoolOr(data, Obfu.raw("loopingAnimationsWhenChangingDisguisesCanSoftlock"), baseConfig.loopingAnimationsWhenChangingDisguisesCanSoftlock),
      loopingAnimationsWhenChangingDisguises: BugfixConfig.getBoolOr(data, Obfu.raw("loopingAnimationsWhenChangingDisguises"), baseConfig.loopingAnimationsWhenChangingDisguises),
      intangibleWhileWarpingEIBombs: BugfixConfig.getBoolOr(data, Obfu.raw("intangibleWhileWarpingEIBombs"), baseConfig.intangibleWhileWarpingEIBombs),
      intangibleWhileWarpingRedBombs: BugfixConfig.getBoolOr(data, Obfu.raw("intangibleWhileWarpingRedBombs"), baseConfig.intangibleWhileWarpingRedBombs),
      intangibleWhileWarpingHits: BugfixConfig.getBoolOr(data, Obfu.raw("intangibleWhileWarpingHits"), baseConfig.intangibleWhileWarpingHits),
      playerPositionAfterWarp: BugfixConfig.getBoolOr(data, Obfu.raw("playerPositionAfterWarp"), baseConfig.playerPositionAfterWarp),
      keepPocketGuuRain: BugfixConfig.getBoolOr(data, Obfu.raw("keepPocketGuuRain"), baseConfig.keepPocketGuuRain),
      raysAtRespawn: BugfixConfig.getBoolOr(data, Obfu.raw("raysAtRespawn"), baseConfig.raysAtRespawn),
      mapScreenIndicators: BugfixConfig.getBoolOr(data, Obfu.raw("mapScreenIndicators"), baseConfig.mapScreenIndicators),
      raysOnHighestLine: BugfixConfig.getBoolOr(data, Obfu.raw("raysOnHighestLine"), baseConfig.raysOnHighestLine),
      teleportersOnHighestLine: BugfixConfig.getBoolOr(data, Obfu.raw("teleportersOnHighestLine"), baseConfig.teleportersOnHighestLine),
      fruitsStuckInColumns: BugfixConfig.getBoolOr(data, Obfu.raw("fruitsStuckInColumns"), baseConfig.fruitsStuckInColumns),
      respawnOfLateralVorticesOnLowestLine: BugfixConfig.getBoolOr(data, Obfu.raw("respawnOfLateralVorticesOnLowestLine"), baseConfig.respawnOfLateralVorticesOnLowestLine),
      fruitsCanJumpIntoAPlatform: BugfixConfig.getBoolOr(data, Obfu.raw("fruitsCanJumpIntoAPlatform"), baseConfig.fruitsCanJumpIntoAPlatform),
      crawlingBadsOnHighestLine: BugfixConfig.getBoolOr(data, Obfu.raw("crawlingBadsOnHighestLine"), baseConfig.crawlingBadsOnHighestLine),
      displayNewLandOnFirstLevel: BugfixConfig.getBoolOr(data, Obfu.raw("displayNewLandOnFirstLevel"), baseConfig.displayNewLandOnFirstLevel),
    });
  }

  private static function getBoolOr(raw: Dynamic, key: String, defaultVal: Bool): Bool {
    if (!Reflect.hasField(raw, key)) {
      return defaultVal;
    }
    var val: Dynamic = Reflect.field(raw, key);
    if (!Std.is(val, Bool)) {
      throw new Error("TypeError: Invalid BugfixConfig field: " + key);
    }
    return val;
  }
}

typedef Options = {
  canBeStuckOnTopOfLateralVortices: Bool,
  startAgainMusicIfSameID: Bool,
  lifeSharingInMulti: Bool,
  reducedDarknessInMulti: Bool,
  consecutiveScoreLevels: Bool,
  lycheesBreakOrder: Bool,
  progressiveLagInLongRuns: Bool,
  strawberriesCanMissEachOther: Bool,
  invisiblePortalsWhenReenteringLevel: Bool,
  loopingAnimationsWhenChangingDisguisesCanSoftlock: Bool,
  loopingAnimationsWhenChangingDisguises: Bool,
  intangibleWhileWarpingEIBombs: Bool,
  intangibleWhileWarpingRedBombs: Bool,
  intangibleWhileWarpingHits: Bool,
  playerPositionAfterWarp: Bool,
  keepPocketGuuRain: Bool,
  mapScreenIndicators: Bool,
  raysAtRespawn: Bool,
  raysOnHighestLine: Bool,
  teleportersOnHighestLine: Bool,
  fruitsStuckInColumns: Bool,
  respawnOfLateralVorticesOnLowestLine: Bool,
  fruitsCanJumpIntoAPlatform: Bool,
  crawlingBadsOnHighestLine: Bool,
  displayNewLandOnFirstLevel: Bool,
}
