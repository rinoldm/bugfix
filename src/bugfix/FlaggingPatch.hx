package bugfix;

import etwin.ds.WeakMap;
import hf.Hf;
import patchman.IPatch;
import patchman.Ref;

class FlaggingPatch implements IPatch {
  private var flags: WeakMap<Hf, Int>;
  private var bits: Int;
  private var inner: IPatch;

  public function new(flags: WeakMap<Hf, Int>, addBits: Int, inner: IPatch) {
    this.flags = flags;
    this.bits = addBits;
    this.inner = inner;
  }

  public function patch(hf: Hf): Bool {
    this.flags.set(hf, this.flags.get(hf) | this.bits);
    return this.inner.patch(hf);
  }
}
